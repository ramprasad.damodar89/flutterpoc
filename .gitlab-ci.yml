image: openjdk:8-jdk

stages:
  - build
  - test
  - package


####################################################################################################
# BUILD
#
.build_template: &build_template_def
  stage: build
  artifacts:
    expire_in: 4 hours
    paths:
    - app/build/outputs/
    - .android/

  before_script:
    # Extract the SDK version that we're building against
    - export ANDROID_COMPILE_SDK=`egrep '^[[:blank:]]+compileSdkVersion'  android/app/build.gradle | awk '{print $2}'`

    # Explict output for logging purpose only
    - echo $ANDROID_SDK_TOOLS
    - echo $ANDROID_COMPILE_SDK

    # Fetch the specified SDK tools version to build with
    - wget --quiet --output-document=/tmp/sdk-tools-linux.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip
    - unzip /tmp/sdk-tools-linux.zip -d .android

    # Set up environment variables
    - export ANDROID_HOME=$PWD/.android
    - export PATH=$PATH:$PWD/.android/platform-tools/

    # Install platform tools and Android SDK for the compile target
    - echo y | .android/tools/bin/sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}"

    - chmod +x ./gradlew

build_debug:
  <<: *build_template_def
  only:
    - develop

  script:
    - ./gradlew assembleDebug

build_release:
  <<: *build_template_def
  only:
    - main
  script:
    - ./gradlew assembleRelease

####################################################################################################
# UNIT TESTING
#

.test_lab_template: &test_lab_template_def
  stage: test
  before_script:
    # Install Google Cloud SDK
    - wget --quiet --output-document=/tmp/google-cloud-sdk.tar.gz https://dl.google.com/dl/cloudsdk/channels/rapid/google-cloud-sdk.tar.gz
    - mkdir -p /opt
    - tar zxf /tmp/google-cloud-sdk.tar.gz --directory /opt
    - /opt/google-cloud-sdk/install.sh --quiet
    - source /opt/google-cloud-sdk/path.bash.inc

    # Setup and configure the project
    - gcloud components update
    - echo $CLOUD_PROJECT_ID
    - gcloud config set project $CLOUD_PROJECT_ID

    # Activate cloud credentials
    - echo $SERVICE_ACCOUNT > /tmp/service-account.json
    - gcloud auth activate-service-account --key-file /tmp/service-account.json

    # List available options for logging purpose only (so that we can review available options)
    - gcloud firebase test android models list
    - gcloud firebase test android versions list


  after_script:
    - source /opt/google-cloud-sdk/path.bash.inc

    # Iterate up to 10 possible test lab configurations and create CLI tool options
    - for i in `seq 1 10`; do export CONFIG="TEST_LAB_DEVICE_CONFIGURATION_$i"; if [ -n "${!CONFIG}" ]; then export TEST_LAB_DEVICE_CONFIGURATIONS="$TEST_LAB_DEVICE_CONFIGURATIONS --device model=${!CONFIG} "; fi; done
    
    # Output for debug logging purpose only
    - echo $TEST_LAB_DEVICE_CONFIGURATIONS
    
    # Run tests if there were any configured
    - if [ -n "$TEST_LAB_DEVICE_CONFIGURATIONS" ]; then gcloud firebase test android run --type robo --app app.apk $TEST_LAB_DEVICE_CONFIGURATIONS --timeout 90s; fi
    
    # Warn if there were no configurations
    - if [ -z "$TEST_LAB_DEVICE_CONFIGURATIONS" ]; then echo "NO FIREBASE TESTLAB CONFIGURATIONS!!!"; fi

test_lab_test_debug:
  <<: *test_lab_template_def
  only:
    - test-lab
  script:
    - cp app/build/outputs/apk/app-debug.apk ./app.apk

test_lab_test_release:
  <<: *test_lab_template_def
  only:
    - main
  script:
    - cp app/build/outputs/apk/app-release.apk ./app.apk



####################################################################################################
# PACKAGE APK FOR DOWNLOADING
#

.package_template: &package_template_def
  before_script:
    - export VERSION_NAME=`egrep '^[[:blank:]]+versionName[[:blank:]]'  app/build.gradle | awk '{print $2}' | sed s/\"//g`
    - export VERSION_CODE=`egrep '^[[:blank:]]+versionCode[[:blank:]]'  app/build.gradle | awk '{print $2}'`
    - mkdir -p deliverables

    # Store some information about the build
    - touch ./deliverables/info.txt
    - echo "Build date          $(date)"                >> ./deliverables/info.txt
    - echo "App version name    ${VERSION_NAME}"        >> ./deliverables/info.txt
    - echo "App version code    ${VERSION_CODE}"        >> ./deliverables/info.txt
    - echo "Git branch          ${CI_COMMIT_REF_NAME}"  >> ./deliverables/info.txt
    - echo "Git commit          ${CI_COMMIT_SHA}"       >> ./deliverables/info.txt
    - echo "Gitlab pipeline     ${CI_PIPELINE_ID}"      >> ./deliverables/info.txt


package_develop:
  <<: *package_template_def
  stage: package
  environment: Development
  only:
    - develop
  script:
    - mv app/build/outputs/apk/app-debug.apk ./deliverables/NameOfTheApp-v$VERSION_NAME-$VERSION_CODE-debug.apk
  artifacts:
    expire_in: 3 days
    paths:
    - deliverables

package_release:
  <<: *package_template_def
  stage: package
  environment: Release
  only:
    - main
  script:
    - mv app/build/outputs/apk/app-release.apk ./deliverables/NameOfTheApp-v$VERSION_NAME-$VERSION_CODE-release.apk
  artifacts:
    expire_in: 4 weeks
    paths:
    - deliverables
